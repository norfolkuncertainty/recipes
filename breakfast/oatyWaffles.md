# Oaty Waffles

## Cooking time

30 minutes

## Servings

5 waffles

## Ingredients

- 3/4 cup of Rolled Oats
- 1 cup Milk
- 1 Egg (Lightly beaten)
- 1/2 tsp Salt
- 2 Tbps Sugar
- 3/4 cup Flour
- 2 tsp Baking powder
- 25g Butter (melted)

## Directions

- Preheat oven to 100 degrees celcius
- Place tray in bottom rack with teflon liner
- Preheat waffle maker
- Add rolled oats to bowl and add Milk
- Add Egg, Salt, Sugar, Flour, Baking Powder, Butter
- Whisk until just combined 
- Add 1/2 cup of batter to waffle maker and cook for 2 1/2 minutes
- Place cooked waffle in the oven
- Repeat with next waffle, try not to stack them in the oven as they will go soggy

Serve with Berries, Nutella, Bacon, Banana, Maple Syrup


