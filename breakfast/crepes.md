# Crepes

## Cooking time

- Prep - 10mins + 12 hours
- Cooking - 20mins

## Servings

6 crepes

## Ingredients

- 1 cup of flour
- 1/2 cup of water
- 1/2 cup of milk
- 1 tsp of vanilla essence
- 1 egg
- 25g of butter (melted)
- pinch of salt

## Directions

### Prep

- Add flour to mixing bowl
- Add all other ingredients and mix thoroughly
- Cover and chill in the fridge over night

### Cooking

- Give the mixture another stir
- Butter non stick pan (preferably a crepe pan) and heat to between 170 and 200 celcius
- Pour 1/3 of a cup of mixture into the pan and spread evenly with a crepe tool
- Cook for 2 minutes and 30 seconds, flip and cook for a further 30 seconds
- Greese pan and check pan temp between crepes

Serve with berries, bacon and banana with maple syrup, lemon and sugar