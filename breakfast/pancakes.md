
# Pancakes

## Cooking time

- Prep - 5mins
- Cooking - 25mins

## Servings

6 Pancakes

## Ingredients

- 1 cup flour
- 2 tsp baking powder
- 2 Tbsp sugar
- 1/2 tsp salt
- 1 cup milk
- 1 egg (lightly beaten)
- 30g butter (melted)

## Directions

- Turn oven on to 100 degrees celcius with a plate on a wire rack on the bottom shelf
- Mix flour, baking powder, sugar, and salt together in a mixing bowl
- Add milk, egg and butter and mix until just combined
- Heat a non-stick frypan to 180-200 celcius and lightly grease with butter
- Add 1/3 of a cup of the mixture to the pan
- Leave cooking until bubbles are bursting on the top and staying open
- Flip and leave for another minute
- Place the pancake in the oven (I like to have a pice of teflon between a double paper towel, this prevents the pancake sticking to the paper towel, or sweating on the plate)
- Repeat with the rest of the mixture

Serve with

- bacon, banana and maple syrup
- berries
