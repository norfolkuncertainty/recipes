# Ginger Beer

## Cooking time

- Prep time 30mins + 5mins per day
- Bottling time 30mins

## Servings

Makes: 1 liter

## Ingredients

### Bug

- 2 1/2 water (Previously boiled ~40 celcius)
- 1 1/2 teaspoons champagne yeast (only for initial batch)
- 1 Tbsp fresh ginger (grated)
- 1 Tbsp sugar
- 2 Tbsp Lemon Juice
- 10g preserved jalapenos

### Daily Feed

- 1 Tbsp Sugar
- 1 Tbsp Fresh Ginger (grated)

### Bottling

- 2/3 cup sugar
- 2L Water (Previously boiled ~40 celcius)

## Directions

### Bug creation

- Add water and yeast into glass jar and stir until combined
- Add sugar, ginger, Lemon Juice and Jalapenos
- Place a breathable cover on top and place in a warm place
- Once a day for a week add the daily feed ingredients to your jar

### Bottling

- Strain bug through a cheese cloth and set 1/2 a cup aside for the next batch
- Divide the bug between two 1.25L plastic bottles
- Mix the Sugar and Water together and fill plastic bottles to the top
- Place in and warm spot, you should be able to feel the bottle getting harder after a few days.
- Refrigerate after a week and enjoy

Repeat bug creation steps ommiting the yeast and substitute 1/2 a cup of water with the previous bug
