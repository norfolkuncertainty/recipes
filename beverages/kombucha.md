# Kombucha

## Cooking time

20mins + 7 days

## Servings

3L

## Ingredients

- 20g Black loose leaf tea
- 3L water (previously boiled)
- 3/4 cup of white sugar
- 1 scoby
- 1 cup of plain kombucha

## Directions

### Prep

- Brew the tea in about 2 cups of hot water in a pyrex vessel and steep for 10 minutes
- Add sugar and stir to dissolve
- Add remaining cold water and stir to ensure sugar is completely dissolved
- Pour through a fine sieve into your fermentation vessel, making sure it removes all the tea
- Add starter tea and scoby when sweet tea is ~35 celcius (just pour on top, and don't mix it. The more acid the surface, the less likely you'll get mould. You may need to turn the scoby the right way up)
- Cover your brew with a finely woven cloth to allow air to get in, but not vinegar flies etc
- Let sit at room temperature for 7 day

### Bottling

- Scoop out the scoby, place it in a small bowl.
- Stir the liquid to distribute the yeasts, and pour a cup or so onto the scoby.
- Pour the rest of the liquid into bottles (filtering through a sieve if you want to remove bits)
- Your kombucha is ready to drink if you like it 'flat'
- Add flavourings, otherwise leave plain and seal bottles 
- Leave at room temperature for at least a few days.
- Chill before drinking

Make a fresh batch using the scoby and reserved tea

## Suggested flavourings

- Citrus and Ginger

## Notes

- A new scoby may form on top of the existing one, this is perfectly normal
  - Either pop it in your compost, or stash it in a scoby hotel (Resources can be found on the internet)