# Banana and Chocolate Chip Muffins

## Cooking time

15 - 20 minutes

## Servings

- 28 mini muffins

or

- 14 normal sized muffins

## Ingredients

- 2 cup Self Raising Flour
- 1/2 cup Caster Sugar
- 1/2 cup Milk
- 2 Eggs
- 60g Butter (melted)
- 250g Fresh or Frozen Banana (mashed)
- 1 cup Dark Chocolate Bits

## Directions

- Preheat over to 180 celcius
- Melt butter if you havent already
- Sift Flour and Sugar into a bowl
- Mix together with a whisk and create a well
- Add Milk, Eggs, Butter and Banana
- Mix until combined
- Stir in chocolate bits
- Spoon into greased muffin trays
- Cook for 15-20mins until golden and tops spring back when pressed
- Transfer to a wire rack to cool

## Notes

- These freeze very well
