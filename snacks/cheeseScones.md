# Cheese Scones

## Cooking time

- Prep - 10 minutes
- Cooking - 8 minutes

## Servings

8 Cheese Scones

## Ingredients

- 320g Self raising flour 
- 100g Grated Tasty cheese
- 57g Butter
- 300g Milk (roughly 1.5 Cups)

## Directions

- Preheat oven to 250 celcius
- Sift Flour into a large bowl
- Grate butter onto flour and mix in with a fork
  - Some stirring and mashing to get it combined
- Stir Grated cheese into mixture
- Add milk and mix until just combined (Do not over mix)
  - If the mixture is too wet, add another 2 Tablespoons of flour
  - If the mixture it too dry, add another 2 Table spoons of milk
- Roll on a floured surface with floured hands until about 2cm thick
- Cut to desired shape
  - Either make a square and then cut into squares using a grid pattern
  - Or use a cutter to make desired shapes and re-form dough until its all gone
- Place Scones on a baking tray and brush with milk
- Bake for 8mins
- Let them sit for 10mins before eating with butter

## Hints

- These are great toasted in the toaster once they get a day or so old