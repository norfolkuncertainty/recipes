# Keto Bread

## Cooking time

- Prep - 10mins
- Cooking - 40mins

## Servings

One loaf - 14 thick slices

## Ingredients

- 230g ground almonds
- 1/2 cup olive oil
- 2 tsp baking powder
- 1/2 tsp fine Himalayan salt
- 1/2 cup water
- 5 large eggs
- 1 tsp chia seeds

## Directions

- Pre-heat oven to 200c
- In a large bowl mix together the almond meal, baking powder and salt.
- While still mixing, drizzle in the oil until a crumbly dough forms.
- Make a well (small hole) in the dough.
- Crack the eggs open into the well.
- Add in the water and beat together, making small circles with your mixer in the eggs until light yellow and frothy. Then begin making bigger circles to incorporate the almond meal mix into it. Keep mixing like this until it looks like pancake batter.
- Pour the mix into a silicon loaf pan, using a spatula to scrape it all out.
- Sprinkle the chia seeds on top.
- Bake for 40 minutes in the center rack.
- Remove from the oven and let it sit for 30 minutes to cool. Then unmold and slice it.
- Store in airtight container in the fridge up to 5 days. Toast to heat.