# Lemon and Almond balls

## Cook time

15 minutes

## Servings

?

## Ingredients

- 4 Tbsp Coconut Oil
- 1 Lemon (juice and zest)
- 1/4 cup Almonds (crushed/ground)
- 1 1/2 cups Dessicated Coconut
- 4 Tbsp Honey

## Directions

- Mix all ingredients together in a bowl
- Take 1 Tbsp of mixture and roll into a ball
- Roll ball in additional dessicated coconut
- Refrigerate
