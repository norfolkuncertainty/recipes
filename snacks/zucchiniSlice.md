# Zucchini Slice

## Cooking time

- 30 minutes

## Servings

- 8-10

## Ingredients

- 5 zucchinis (grated, can substitute with some carrot)
- 5 eggs
- 1 cup self raising flour
- 1/3 cup oil
- 1 cup cheese (grated)
- handful of ham (chopped)
- salt and pepper (optional)

## Directions

- Heat oven to 180 degrees celcius fan bake
- Combine all ingredients in a large bowl, mix well
- Add mixture to a greased / lined slice tin
- Cook for 30 minutes or until firm and golden