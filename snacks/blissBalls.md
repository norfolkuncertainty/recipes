# Bliss Balls

## Cooking time

10 Minutes

## Servings

50 Balls

## Ingredients

- 325g Pitted Dates
- 100g Raw Cashews
- 50g Cocoa Powder
- 25g Dried Goji Berries

## Directions

- Place all ingredients in food processor
- Blend until all ingredients are well blitzed
- Take 1 Tbsp of mixture and compress into a ball in your hand
- Store in the fridge
