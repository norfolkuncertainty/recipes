# Cheese Rolls

## Cooking time

20 minutes

## Servings

20 Cheese Rolls

## Ingredients

- 1 Loaf White Sanwhich Bread
- 500g Cheese
- 2 tsp Vinegar
- 1 tsp Sugar
- 1 tsp Mustard
- 1 1/2 Tbsp Maggie Onion soup mix
- 3/4 cup Boiling Water

## Directions

- Preheat oven to 200 celcius
- Mix all ingedients well in a glass bowl
- Place 1 Tbsp of mixture in the centre of bread and roll up
- Bake for 10 minutes