# Kumara Banana Cookies

## Cooking time

- Prep 5 minutes
- Cooking 35 minutes

## Servings

- Approx 20 cookies

## Ingredients

- 250g orange kumara
- 1 Tbsp chia seeds
- 2 Tbsp water
- 100g banana
- 1 cup rolled oats
- 1/4 cup plain flour
- 1/2 tsp cinnamon

## Directions

- Heat oven to 180 degrees
- Peel, cube and boil kumara for approx 10 minutes
- Once cooked, drain and leave to sit in warm pot to allow water to evaporate
- Add chia seeds and water to a small bowl and leave to stand for 10-15 minutes (should begin to gel)
- Add bananas to the cooked kumara, mash together
- Stir through the soaked chia seeds
- Add the oats, flour and cinnamon, mix well
- Scoop tablespoons of mixture and roll into balls
- Place the balls on a lined baking tray
- Give each ball a light squish with a wet fork
- Bake for approx 20 - 25 minutes
- Allow to cool before serving
- Store in an airtight container for 2-3 days or freeze