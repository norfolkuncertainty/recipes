# Banana Spinach Muffins

## Cooking Time

- 20 - 25 minutes

## Servings

- 12-15 approx

## Ingredients

- 250g banana (approx 2 - 2.5)
- 100g spinach
- 150ml milk
- 2 Tbsp maple syrup
- 1 egg
- 50ml canola oil or melted butter
- 1 cup rolled oats
- 1 cup flour
- 2 tsp baking powder

## Directions

- Heat oven to 160 degrees fan bake
- Blend banana, spinach and milk together in food processor
- Pour into a large bowl
- Add maple syrup, egg and oil/butter and whisk together
- Add rolled oats, flour and baking powder and mix with a wooden spoon
- Fill greased muffin tins
- Bake for 20-25 minutes
