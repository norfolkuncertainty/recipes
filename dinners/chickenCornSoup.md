# Chicken Corn Soup

## Cooking time

30mins

## Servings

Serves 4-6

## Ingredients

- 6 cups chicken stock
- 1 large chicken breast fillet (boneless)
- 300 g canned corn kernels
- 300 g canned creamed corn
- 1 tablespoon cornflour (cornstarch)
- 2 tablespoons light soy sauce
- 2 eggs (beaten)

## Directions

- Bring the stock to the boil in a large saucepan(depending on your taste or needs you may wish to substitute some of the stock for water).
- Add chicken breast fillet to the stock, reduce to a simmer for 20 minutes.
- Remove chicken breast from the stock and leave to cool for a few minutes then shred.
- Add corn to stock and bring to the boil over a medium heat.
- Combine soy sauce and cornflour into a paste then stir into the soup to thicken slightly.
- Add shredded chicken to soup.
- Slowly pour beaten eggs into the soup in a steady stream, stirring constantly with a fork.