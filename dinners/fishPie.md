# Fish Pie

## Cooking time

30 minutes

## Servings

Serves 4

## Ingredients

- 2 Eggs (chopped)
- 1 cup Pasta
- 1 cup Frozen Vege
- 500g Smoked fish (flaked)
- 3 Tbps butter
- 3 Tbsp Flour
- 3 cups Milk
- 500g Watties Pom Poms

## Directions

- Preheat oven to 180 celcius
- Pot 1
  - Hard boil Eggs (15mins)
- Pot 2
  - Cook pasta for 8 mins in lightly salted water (8mins)
- Pot 3
  - Melt butter over a low heat
  - Remove from heat and whisk in flour
  - Return to heat and slowly add milk while constantly stiring
  - Heat until thickened

- Mix Fish, Eggs, Frozen Vege, Pasta, and White sauce in a ceramic dish
- Top with Pom Poms and bake for 30 - 40 minutes
