# One Pot Chicken Vegetable Parmesan Orzo

## Cooking time

- Prep time 10 mins
- Cook time 20mins 

## Servings

2 - 3

## Ingredients

- 1 Tbsp olive oil
- 1 Onion (diced)
- 2 Garlic Cloves (crushed)
- 1 Carrot (diced)
- 1 Capsicum (diced)
- 2 cups Spinach (roughly chopped, lightly packed)
- 1/4 cup frozen peas
- ~300g chicken thigh or breast (cut into bite size pieces)
- 1 cup Orzo / Risoni
- 1 Tbsp flour
- 1 1/2 cups Milk
- 2 cups chicken stock
- 3/4 cup Parmesan (grated)
- Salt and pepper

## Directions

- Heat oil in  large heavy based pan with lid (At least 5cm deep)
- Add Onion and cook until lightly browned
- Add Garlic and Carrot and cook for 1 minute, stirring occasionally
- Add Capcicum and Chicken and cook until Chicken is cooked on the outside
- Add Milk, Stock, Orzo, and Flour and mix well
- Place on lid and bring to the boil
- Turn down heat and cook at a rapid simmer for 5 mins, stiring once or twice
  - If the mixture looks like it is drying out, add some water
- Add Peas, Spinach and Parmesan and stir through 
- Season with salt and pepper to taste
- Cook until Chicken is cooked through
