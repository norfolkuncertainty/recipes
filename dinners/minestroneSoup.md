# Minestrone soup

## Cooking Time

30mins

## Servings

Serves 4

## Ingredients

- 2 Rashers bacon (chopped)
- 1 Onion (finely chopped)
- 2 Stalks celery (sliced)
- 1 tsp dried oregano
- 1 litre chicken stock
- 400g can of Tomatoes
- 4 cups chopped vegetables - potato, carrot, courgettes, green beans
- 400g can of Red Kidney Beans (drained and rinsed)
- 2 Tbsp chopped fresh parsley or basil

## Directions

- Heat a dash of oil in a large saucepan. Add the bacon, onion, celery and cook over medium heat until softened.
- Add the oregano, chicken stock, Tomatoes and chopped vegetables to the saucepan. Bring to the boil
- Cover and simmer for 15 minutes.
- Add the Red Kidney Beans and return to the boil. Simmer for a further 3 to 4 minutes or until the beans are hot. 
- Stir in the parsley or basil and season to taste with salt and pepper.