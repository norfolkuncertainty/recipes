# Pumpkin Soup

## Cooking time

- Prep 10 minutes
- Cooking 20 minutes

## Servings

2L of soup

## Ingredients

- 1 Crown Pumpkin (small, chopped)
- 1 Purple Kumara (medium sized, chopped)
- 2 Potatoes (medium sized, chopped)
- 3 chicken stock cubes (Mixed with enough water to cover 3/4 of veges)
- Salt

## Directions

- Add all ingredients to pot
- Bring to boil and simmer for 20mins
- Whizz with stick blender
- Season to taste

## Notes

- Paler grey skinned pumpkins are better
