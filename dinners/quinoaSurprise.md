# Quinoa Suprise (the suprise is that you cant taste the quinoa)

## Cooking time

30mins

## Servings

2

## Ingredients

- 1/2 Cup Quinoa (rinsed)
- 1/2 Cup Water
- 1 tsp Beef stock
- 500g Beef Mince
- 2 Tbsp oil
- 1 Onion (diced)
- 1 Clove Garlic (crushed)
- Spice paste
  - 1/2 tsp Cumin
  - 1/2 tsp Turmeric
  - 1/2 tsp Paprika
  - 1/2 tsp Ground chilli
  - 1/2 tsp Garam masala
  - 1 Tbsp Water
- 2 cups Frozen Mixed Veggies
- 1 Can Whole peeled tomatoes
- 1/2 tsp Salt
- Pepper to taste

## Directions

### Quinoa

- Bring Quinoa, water and stock to the boil in a covered pot and simmer for 10mins on low heat
- Leave covered off the heat for another 10mins

### Mince Mixture

- Mix spices with water into a paste
- Heat Oil in fry pan
- Cook onion and garlic in frying pan on medium heat for 5mins or until soft
- Add spice paste and stir in for 30 seconds
- Add beef mince and break up into small pieces as it cooks
- Add can of tomatoes and break up
- Microwave frozen veggies on high in microwave proof dish for 5mins (depending on power of microwave) and add to mince mixture
- Add cooked quinoa into mince and veggie mixture and mix in well
- Leave simmering for 5mins
- Season to taste
