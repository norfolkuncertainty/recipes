# Devilled Sausages

## Cooking time

30mins

## Servings

4

## Ingredients

- 500g beef or pork sausages
- 1 tablespoon olive oil
- 1/2 cup water
- 1 onion (chopped)
- 1 tsp soy sauce
- 2 Tbsp Worcestershire sauce
- 1 tsp mustard
- 1⁄4 tsp cayenne pepper
- 2 Tbsp tomato paste
- 1 Tbsp brown sugar
- 1 peeled/cored and grated apple

### Optional

- Potato
- Rice
- Kumara

## Directions

- Heat oil in pan and brown sausages
- Remove sausages from pan and slice into bite size chunks, return to pan
- Add onion, cook until browned
- Combine all other ingredients with the water to make a sauce
- Add sauce to pan and stir until sauce begins to boil
- Reduce heat and simmer for 10mins, stirring occasionally
- Serve with either a potato/kumara mash or rice