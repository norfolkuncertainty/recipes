# Chicken Pajamas

## Cooking time

50mins

## Servings

Serves 4

## Ingredients

- 2 Chicken Breasts
- 1 egg
- 1/2 cup Panko breadcrumbs
- 1/2 cup grated Parmesan Cheese
- 1/2 tsp Salt
- 1 teaspoon Pepper
- 1 teaspoon Garlic Powder
- Olive Oil Spray
- 1/2 tin Watties Garden Vegetable Pasta Sauce
- Approx 60g Mozzarella Cheese
- Fresh Basil (optional)
- 4 potatos cut into wedges or chips
- 1 carrot cut into sticks
- 1 brocolli cut into florets

## Directions

- Preheat oven to 200 degrees fan bake.
- Prep wedges or chips, canola oil spray and salt (cook for 40-45 minutes, turning once).
- Fillet the chicken breasts in half.
- Whisk egg in a shallow bowl. 
- In a separate bowl, add the next 5 ingredients.
- Dip the chicken in the egg, let it drip off then dip it in the breadcrumb mix.
- Place the chicken breasts on a baking tray, spray with olive oil and bake for 30 minutes.
- With 10mins remaining, start steaming vegetables
- Spread the pasta sauce over the chicken, sprinkle the mozzarella cheese on top and grill for 5 minutes.


