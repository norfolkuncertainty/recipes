# Katsu Chicken Curry

## Cooking Time

1h

## Servings

Serves 4

## Ingredients

Curry Sauce:
- 1 Tbsp Olive Oil
- 1 Onion sliced or diced
- 1 Carrot peeled and sliced into half rounds
- 3 cups boiling water
- 1 Packet of Golden Curry Mix 92g

Crumbed Chicken:
- 1 Tbsp Olive Oil
- 500g Chicken Breast (2 breasts)
- 1/4 cup plain flour
- 1 egg
- 1 cup Panko Crumbs

Serve with:
- 2 cups of cooked rice
- 1 cup shredded cabbage
- Japanese Mayo

## Directions

Cook Rice

To make curry sauce:
- Saute onion in olive oil in a large pot until soft. Add carrot and continue cooking.
- Add water and bring to the boil.
- Add Golden Curry Mix and stir to dissolve.
- Simmer for 15 minutes and keep stirring.

Chicken:
- Cut chicken breasts in half through the centre and bash with rolling pin to make flat/even thickness.
- Coat chicken in flour, then egg, then breadcrumbs.
- Heat olive oil in a pan and fry chicken on each side until golden and cooked through.

- Slice the cooked chicken and serve on top of cooked rice and shredded cabbage. Pour over the curry sauce and add Japanese mayo.

