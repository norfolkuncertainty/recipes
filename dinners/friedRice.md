# Fried Rice

## Cooking time

15mins

## Servings

4

## Ingredients

- 2 eggs (beaten)
- 2 Rashers of Bacon (cut into small piece)
- 1 cup Cooked rice
- 1 Onion (diced)
- 2 cloves of garlic (Minced)
- 1 Tbsp ginger (finely grated)
- 300g Chicken (cut into chunks)
- 1 cup Frozen mixed veggies
- 3 Tbsp tamari/soy sauce

## Directions

This recipe works best if you have a wok and small nonstick frying pan

- If possible, cook the rice in the morning/night before and leave in the fridge

### Frypan

- Heat oil in pan
- Make omlette with eggs, then finely slice and put aside
- Cook bacon and put aside

### Wok

- Heat oil
- Cook onion until soft 
- Add garlic and ginger and cook breifly, then put aside
- Cook chicken and put aside
- Cook frozen veggies in wok
- Add chicken, bacon, onion, garlic and ginger and toss together
- Add rice and soy sauce
- Mix thoroughly ensuring the rice doesnt stick
- Stir egg through and serve