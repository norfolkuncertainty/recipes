# Beef Nachos:

## Cooking time

30mins

## Servings

Serves 4-6

## Ingredients

- 1 tablespoon olive oil
- 1 onion (diced)
- 1 carrot (grated)
- 500g beef mince
- 1 teaspoon ground cumin
- 1 teaspoon ground coriander
- 1 teaspoon smoked paprika
- 1/4 teaspoon oregano
- 1/4 teaspoon garlic powder
- 1 tablespoon tomato paste
- 1 can mild chilli beans or red kidney beans (drained)
- corn kernals (optional)
- corn chips
- cheese
- sour cream
- sweet chilli sauce or sriracha sauce

## Directions

- Add the olive oil to a large frypan and cook the onion on medium until softened.
- Add carrot and cook until softened.
- Turn up the heat to high, add mince and cook until browned.
- Add spices with tomato paste and stir to combine.
- Add the chilli or kidney beans and corn kernals and stir until warmed through. 
- Serve in bowls topped with corn chips, cheese, sour cream and sauce.
