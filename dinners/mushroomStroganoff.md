# Mushroom Strognaoff

## Cooking time

- 40 minutes

## Servings

- 4 serves

## Ingredients

- 1 cup Brown Rice
- 1 1/2 cups of Water
- 1 tsp Dried Thyme
- 1 Carrot (chopped)
- 1 Capcicum (sliced)
- 500g Button Mushrooms (sliced)
- 1 can of Canollini Beans (drained and rinsed)
- 400g can of Chopped Tomatoes
- 1/2 cup Coconut Cream
- 2 tsp Brown Sugar
- 1/2 tsp Salt

## Directions

### Rice

- Place Brown rice and water into a small pot, and bring to the boil
- Reduce heat to medium, and cook for 20mins
- Reduce heat to low, and cook for a further 20mins

### Stroganoff

- Add Thyme with some oil to a large pot, and put on a medium to high heat
- Add Carrot and Capcium, Cook until Soft
- Add Mushrooms and Canollini Beans and stir
- Add Tomatoes, Coconut Cream, Brown Sugar and Salt to a blender and bend till smooth
- Add Sauce to pot and simmer for 5mins or until rice is ready
- Serve Strognaoff ontop of Rice