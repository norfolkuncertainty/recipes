# Cottage Pie

## Cooking time

- Prep - 1 hour 15 minutes
- Cooking - 30 minutes

## Servings

6 people

## Ingredients

### Pie filling

- olive oil
- 1kg beef mince
- 1 onion (finely diced)
- 2 carrots (finely diced)
- 2 stalks celery (finely diced)
- 400g can chopped tomatoes
- 1 Tbsp tomato paste
- 250ml beef stock
- 2 Tbsp fresh parsley (finely chopped)
- 2 tsp fresh thyme leaves (finely chopped)
- (can substitute fresh herbs for dry herbs)
  
### Potato topping

- 1.2kg agria potato (peeled and chopped)
- 200mls milk
- 25g butter
- 1 cup grated cheese
- 1 tsp paprika

## Directions

Preheat oven to 190 celcius

### Pie filling

- Heat oil in a large pot on medium-high heat
- Add the mince and brown
- Remove and set aside
- Reduce the heat to low
- Add another dash of oil
- Cook onion for at least 5 minutes until soft
- Increase heat
- Add carrot and celery and cook for a further 5 minutes
- Return mince to the pot with tomatoes, tomato paste and stock
- Bring to the boil, season as required
- Reduce heat, cover and gently simmer for 30 minutes
- Stirring occasionally until most of the liquid has evaporated
- Stir through the chopped herbs

### Potato Topping

- Boil potatoes in lightly salted water until tender
- Drain and dry over heat
- Mash potato
- Beat in the milk and butter
- Stir through the cheese

### To assemble

- Spoon mince mixture into a large ovenproof dish (6 cup capacity)
- Top with the potato topping
- Sprinkle on paprika
- Cook in the oven for 30 minutes until hot and bubbling
