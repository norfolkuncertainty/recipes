# Bean and Rice Burrito

## Cooking time

- 25 minutes

## Servings

- 8 burritos (approx 2-3 per person)

## Ingredients

- 1-1/2c water
- 1c uncooked brown rice
- 1 tablespoon olive oil
- 1 capsicum (diced)
- 1 onion (diced)
- 1 clove of garlic (crushed)/ 1 teaspoon crushed garlic
- 1 teaspoon chilli powder
- 1 teaspoon cumin
- 1 teaspoon ground coriander
- 1 teaspoon smoked paprika
- 1/4 teaspoon dried oregano
- 1/8 teaspoon garlic powder
- 1 can of black beans (rinsed and drained)
- 8 round tortilla wraps
- salsa (to serve)
- grated cheese (to serve)
- sour cream (to serve)

## Directions

- Put rice and water in a pot and cover with lid
- Turn heat to full power
- Once boiling, turn down to medium heat and cook for 20 minutes
- Turn down to low heat and cook for a further 20 minutes
- Meanwhile, in a large fry pan, heat the oil and saute capsicum and onion until tender
- Add the garlic and cook for 1 minute
- Stir in the spices until combined
- Add beans and rice, cook and stir until heated through
- Warm tortilla wraps on both sides in a dry fry pan
- Spoon about 1/2 cup of filling into each tortilla, top with salsa, cheese, sour cream, wrap and serve