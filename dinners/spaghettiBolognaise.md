# Spaghetti Bolognese

## Cooking time

- 50 minutes

## Servings

- 4 Serves

## Ingredients

### Pangrattato

- 1/2 Cup Panko Break Crumbs
- 1 Clove Garlic (crushed)
- 1 Tbsp Olive Oil

### Spice Mix

- 1 Tbsp Sun dried tomatoes (finely chopped)
- 1/2 tsp dried Oregano
- 1/2 tsp dried Basil
- 1/2 tsp Garlic Powder
- 1/2 tsp Onion Powder
- 2 tsp brown sugar


### Beef Bolognese

- 1 Brown Onion (diced)
- 500g Beef Mince
- 1 Clove Garlic
- 1 Courgette (grated)
- 1 Pottle Tomato paste 
- 400g Chopped Tomatoes (tinned/fresh)
- 1 tsp Salt
- 1 tsp Balsamic Vinegar
- 1 tsp Beef Stock Powder
- 50g Spinach (finely sliced)

### To Serve

- 400G Dried Spaghetti
- 50g Feta Cheese

## Directions

## Pangrettato

- Mix all ingredients in a bowl
- Cook over a medium heat for 3-5 mins until golden

## Bolognese

- Heat some oil in a pan, and add Onion - cook until soft and lightly browned
- Add Garlic and Beef, breaking up into small pieces, cook until browned
- Add Courgette, Spice Mix, Tomato Paste, cook until Courgette is softened
- Add Tomatoes, Salt, Balsamic Vinegar, and Beef stock
- Bring to a Simmer
- Boil a pot of water with salt and some oil
- Add Spaghetti and cook for 12 minutes
- Stir Spinach into Bolognese just before serving.
- Serve Bolognese on top of pasta, topped with Pangrettato and Feta
