# Mexican Chicken

# Cooking time

30mins

## Servings

Serves 2

## Ingredients

- 1 cup Liquid Chicken Stock
- 1/2 cup Basmati rice
- 1/2 tsp Tumeric
- 1 Tbsp Southwest Spice blend
- 1 Tbsp Olive oil
- 300g Chicken breast (thinly sliced)
- 1 x Capsicum (thinly sliced)
- 1 x Lime (zest and cut into quarters)
- Salt & Pepper
- 2 Tbsp Butter
- Sour Cream
- Medium Salsa

### Southwest Spice Blend:

- 2 tablespoon Chili powder
- 2 teaspoon ground Cumin
- 2 tablespoon Paprika
- 1 teaspoon Black Pepper
- 1 tablespoon ground Coriander
- 1 teaspoon Cayenne Pepper
- 1 tablespoon Garlic Powder
- 1 tablespoon Salt
- 1 tablespoon Dried Oregano

## Directions

- Add rice, tumeric and chicken stock to a pot and cook for 20 minutes. (10min simmer/10mins warm)
- Add olive oil to a pan over medium heat, add the capsicum and season with 1/2 tablespoon of the Southwest Spice, salt and pepper. Cook until slightly softened, 2-3 minutes.
- Pat chicken dry and season with another 1/2 tablespoon of Southwest Spice, salt and pepper.
- Add the chicken to the pan and stir until cooked through. 
- Stir in 1 Tbsp of butter until melted, remove from heat.
- Once cooked, fluff the rice with a fork and stir in 1 Tbsp of butter until melted.
- Season rice with salt and pepper. 
- Serve rice topped with chicken and capsicum.
- Add lime zest and juice to sour cream and put on top with salsa.
