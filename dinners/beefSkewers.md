# Beef Skewers

## Cooking time

5 minutes

## Servings

6 Skewers

## Ingredients


- 1 Tbsp cornflour
- 1/2 tsp salt
- 1/2 tsp ground cumin
- 1/2 tsp dried coriander
- 1/2 tsp paprika
- 1/2 tsp ground turmeric
- 1/2 tsp ground cinnamon
- 1/4 tsp ground cloves
- 1/4 tsp cayenne
- 500g Rump Beef (cubed)
- 6 bamboo skewer (soaked in water)
- 1 Tbsp vegetable oil

## Directions

- Heat BBQ on high for 10 mins
- Mix all spices together in a small dish and set aside
- Evenly slide beef onto skewers
- Coat beef in oil
- Coat beef with spice mix
- Turn BBQ down to medium and cook for 5 mins turning once

## Notes

- Serve with 
  - Slivered almond and dried apricot couscous
  - Tzatziki