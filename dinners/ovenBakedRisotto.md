# Oven baked mushroom risotto

## Cooking time

1 hour

## Servings

Serves 4

## Ingredients

- 50g Butter
- 1 Onion (finely chopped)
- 300g Button Mushrooms (chopped)
- 300g Aborio Rice
- 4 cups Vegetable stock
- 1 Tbsp White wine vinegar
- 4 Tbsp Parmesan cheese (grated)
- 2 Tbsp Italian parsley (chopped)

## Directions

- Preheat oven to 180 celcius
- Melt half the butter in a large heavy based oven safe pan over a medium heat
- Add onion and cook for 5 minutes
- Add mushrooms and cook for another 5 minutes
- Add remaining butter and stir in rice, coating it well with butter and cook for 2 minutes
- Add stock and White wine vinegar, mix well
- Bring to the boil
- Transfer pot to oven and bake for 25 to 30 minutes
- Fluff rice with a wooden spoon and add parmesan
- Bake for a further 5 minutes
- Rest for 5mins, then stir in parsely
- Serve topped with parmesan
