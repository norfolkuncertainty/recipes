# Toothpaste

## Cooking time

5 minutes

## Servings

N/A

## Ingredients

- Salt
- Baking Soda

## Directions

- Mix at a ratio of 1 part salt to 5 parts baking soda
  - ie 1 tsp salt with 5 tsp baking soda
- Wet toothbrush and dip in mixture