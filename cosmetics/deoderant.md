# Deoderant

## Cooking time

35 mins

## Servings

N/A

## Ingredients

- Coconut oil
- Baking Soda
- Corn Flour

## Directions

- Mix at a ratio of 5 parts coconut oil to 1 part Baking soda and 1 part Corn flour
  - ie 5 tsp Coconut oil, 1 tsp Baking soda, 1 tsp Corn flour
- Place in fridge after well mixed for 30mins
- Keep at room temperature (Depending on your climate)
- Apply a small amount to armpits directly