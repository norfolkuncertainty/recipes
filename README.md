# Recipes

## What is this?

This is a collection of recpies frequently used by our family.
I created this repo because I had recpies all over the place, and wanted a central place to go.

## What you will need

- Spoon measurements
- Cup measurements
- Scales
- Infrared thermometer
- Non-stick frypan
- Wok
- Large Pot

## Dietary requirements

There are some dairy and gluten free recipes in here, but most can be adapted with substitution

## Contributing

- Pull requests are welcome
- Create your recipe based on the template.md in the root of the repo
