# Almond Butter

## Cooking time

25 mins

## Servings

500g Jar

## Ingredients

- 500g Raw Almonds (Also known as Desser Almonds)

## Directions

- Preheat oven to 200 celcius
- Place almonds in a single level on a baking tray
- Bake almonds for 10mins
- Remove from oven and transfer into food processor
- Blend on a medium speed for around 10mins
  - The almonds will tend to clump up the sides, either stop the processor and scrap from the sides, or stick a rubber scraper through the top hole
- Blend until until it is around 90 degrees celcius (at this point it should be oily and smooth)
- Transfer into clean glass jar

Store in the fridge and consume within two weeks
